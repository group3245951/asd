<?php
include '../capstone/admin/inc/db_config.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get and sanitize user inputs
    $name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $number = filter_var($_POST["number"], FILTER_SANITIZE_STRING);

    if (empty($name)) {
        echo "Name is required.";
    } else {
        // Insert user data into the "user" table
        $sql = "INSERT INTO user (name, number) VALUES ('$name', '$number')";
        if ($con->query($sql) === TRUE) {
            echo "You have Successfully Booked! Wait for a message confirmation regarding with you booking.";
            
            // Add a button to redirect to "bookings.php"
            echo "<a href='./bookings.php'><button>Go To Bookings</button></a>";
        } else {
            echo "Error: " . $sql . "<br>" . $con->error;
        }

        $con->close();
    }
}
?>