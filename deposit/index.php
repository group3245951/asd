<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Redirect Page</title>
  <style>
    /* Center the loading animation */
    body {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
      margin: 0;
    }

    /* Define the loading animation */
    .loading {
      border: 16px solid #f3f3f3; /* Light gray */
      border-top: 16px solid #3498db; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
    }

    /* Animation for the loading spinner */
    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }

    /* Style for the text */
    .text {
      position: absolute;
      top: 20px;
      left: 20px;
      font-size: 20px;
      line-height: 1.5;
    }
  </style>
  <script>
    // Redirect to the next page after 5 seconds
    setTimeout(function() {
      window.location.href = "test.php";
    }, 5000); // 5000 milliseconds = 5 seconds
  </script>
</head>
<body>
  <div class="text">
    <h1>Please do not refresh this page...</h1>
    <p>This page will automatically redirect to the next page in 5 seconds.</p>
  </div>

  <div class="loading"></div> <!-- Add the loading spinner -->

</body>
</html>
