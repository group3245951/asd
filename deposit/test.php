<!DOCTYPE html>
<html>
<head>
<style>
    .modal {
      display: none;
      position: fixed;
      z-index: 1;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      overflow: auto;
      background-color: rgba(0, 0, 0, 0.5);
    }

    .unclickable-form {
      max-width: 400px;
      margin: 0 auto;
      font-family: Arial, sans-serif;
      font-size: 14px;
    }

    .form-group {
      margin-bottom: 20px;
    }

    label {
      display: block;
      margin-bottom: 5px;
      font-weight: bold;
    }

    input[type="text"],
    input[type="number"] {
      padding: 8px;
      margin-bottom: 10px;
      border-radius: 3px;
      border: 1px solid #ccc;
      width: 85%;
    }

    input[readonly] {
      background-color: #f5f5f5;
      cursor: not-allowed;
    }

    input[type="number"]:required:invalid {
      border-color: red;
    }

    input[type="number"]:required:valid {
      border-color: #ccc;
    }

    .modal-content {
      background-color: #fefefe;
      margin: 5% auto;
      padding: 20px;
      border: 1px solid #888;
      width: 40%; /* Adjusted width */
      max-width: 400px; /* Added max-width */
      text-align: center;
    }

    .close {
      color: #aaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }

    .openModalBtn {
      padding: 10px 20px;
      background-color: #4CAF50;
      color: white;
      border: none;
      border-radius: 4px;
      font-size: 16px;
      cursor: pointer;
      margin: 5px;
    }

    .openModalBtn:hover {
      background-color: #45a049;
    }

    .openNestedModalBtn {
      padding: 10px 20px;
      background-color: #4CAF50;
      color: white;
      border: none;
      border-radius: 4px;
      font-size: 16px;
      cursor: pointer;
      margin: 5px;
    }

    .openNestedModalBtn:hover {
      background-color: #45a049;
    }

    .ok-button {
      padding: 10px 20px;
      background-color: #4CAF50;
      color: white;
      border: none;
      border-radius: 4px;
      font-size: 16px;
      cursor: pointer;
    }

    .ok-button:hover {
      background-color: #45a049;
    }
</style>
</head>
<body>

<div style="text-align: center;">
  <h1>GCash Options</h1>
  <button class="openModalBtn" data-modal="loginModal1">Gcash - DD</button>
  <button class="openModalBtn" data-modal="loginModal2">Gcash - WJ</button>
  <button class="openModalBtn" data-modal="loginModal3">Gcash - CUB</button>
</div>

<div style="text-align: center;">
  <h1>Paymaya Options</h1>
  <button class="openModalBtn" data-modal="PaymayaModal1">Paymaya - DD</button>
  <button class="openModalBtn" data-modal="PaymayaModal2">Paymaya - WJ</button>
  <button class="openModalBtn" data-modal="PaymayaModal3">Paymaya - CUB</button>
</div>

<div style="text-align: center;">
  <h1>Cryptocurrency Options</h1>
  <button class="openModalBtn" data-modal="CryptoModal1">
  <img src="images/btc.png" alt="Paymaya - DD" style="width: 50px; height: 50px;">
  </button>
  <button class="openModalBtn" data-modal="CryptoModal2">
  <img src="images/usdt.png" alt="Paymaya - DD" style="width: 50px; height: 50px;">
  </button>
  <button class="openModalBtn" data-modal="CryptoModal3">
  <img src="images/eth.png" alt="Paymaya - DD" style="width: 50px; height: 50px;">
  </button>
</div>


<!-- Gcash Section -->
<div id="loginModal1" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
      </div><br>
        <label for="accountName1">Account Name:</label>
        <input type="text" id="accountName1" name="accountName1" readonly value="John Doe">
      </div>
      <div class="form-group">
        <label for="accountNumber1">Account Number:</label>
        <input type="text" id="accountNumber1" name="accountNumber1" readonly value="123456789">
      </div>
      <div class="form-group">
        <label for="amountToPay1">Amount to Pay:</label>
        <input type="number" id="amountToPay1" name="amountToPay1" required>
      </div>
      <input type="checkbox" id="termsCheckbox" required>
      <label for="termsCheckbox">I accept the terms and conditions</label>
    </form>
    <button class="openNestedModalBtn" onclick="openNestedModal()">Open Nested Modal</button>

    <div id="nestedModal1" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 1</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique iste, doloremque nemo tempora ducimus rerum error id perferendis, illo mollitia eligendi voluptates autem impedit dolorem officia voluptatibus, cumque amet?</p>
        <button id="nestedModalOkButton1" class="ok-button" onclick="acceptTerms()">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="loginModal2" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName2">Account Name:</label>
        <input type="text" id="accountName2" name="accountName2" readonly value="Jane Smith">
      </div>
      <div class="form-group">
        <label for="accountNumber2">Account Number:</label>
        <input type="text" id="accountNumber2" name="accountNumber2" readonly value="987654321">
      </div>
      <div class="form-group">
        <label for="amountToPay2">Amount to Pay:</label>
        <input type="number" id="amountToPay2" name="amountToPay2" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal2" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 2</h2>
        <p>This is the nested modal 2.</p>
        <button id="nestedModalOkButton2" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="loginModal3" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName3">Account Name:</label>
        <input type="text" id="accountName3" name="accountName3" value="Sarah Johnson" readonly>
      </div>
      <div class="form-group">
        <label for="accountNumber3">Account Number:</label>
        <input type="text" id="accountNumber3" name="accountNumber3" value="555555555" readonly>
      </div>
      <div class="form-group">
        <label for="amountToPay3">Amount to Pay:</label>
        <input type="number" id="amountToPay3" name="amountToPay3" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal3" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 3</h2>
        <p>This is the nested modal 3.</p>
        <button id="nestedModalOkButton3" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Paymaya Section -->
<div id="PaymayaModal1" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName1">Account Name:</label>
        <input type="text" id="accountName1" name="accountName1" readonly value="Allan">
      </div>
      <div class="form-group">
        <label for="accountNumber1">Account Number:</label>
        <input type="text" id="accountNumber1" name="accountNumber1" readonly value="1233123123">
      </div>
      <div class="form-group">
        <label for="amountToPay1">Amount to Pay:</label>
        <input type="number" id="amountToPay1" name="amountToPay1" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal1" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 1</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique iste, doloremque nemo tempora ducimus rerum error id perferendis, illo mollitia eligendi voluptates autem impedit dolorem officia voluptatibus, cumque amet?</p>
        <button id="nestedModalOkButton1" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="PaymayaModal2" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName2">Account Name:</label>
        <input type="text" id="accountName2" name="accountName2" readonly value="Sheesh">
      </div>
      <div class="form-group">
        <label for="accountNumber2">Account Number:</label>
        <input type="text" id="accountNumber2" name="accountNumber2" readonly value="4534242">
      </div>
      <div class="form-group">
        <label for="amountToPay2">Amount to Pay:</label>
        <input type="number" id="amountToPay2" name="amountToPay2" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal2" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 2</h2>
        <p>This is the nested modal 2.</p>
        <button id="nestedModalOkButton2" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="PaymayaModal3" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName3">Account Name:</label>
        <input type="text" id="accountName3" name="accountName3" value="Andrea" readonly>
      </div>
      <div class="form-group">
        <label for="accountNumber3">Account Number:</label>
        <input type="text" id="accountNumber3" name="accountNumber3" value="543534" readonly>
      </div>
      <div class="form-group">
        <label for="amountToPay3">Amount to Pay:</label>
        <input type="number" id="amountToPay3" name="amountToPay3" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal3" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 3</h2>
        <p>This is the nested modal 3.</p>
        <button id="nestedModalOkButton3" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>


<!-- Crypto Section -->
<div id="CryptoModal1" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
         </div><br>
      </div>
      <div class="form-group">
        <label for="accountNumber1">Account Address:</label>
        <input type="text" id="accountNumber1" name="accountNumber1" readonly value="1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2">
      </div>
      <div class="form-group">
        <label for="amountToPay1">Amount to Pay:</label>
        <input type="number" id="amountToPay1" name="amountToPay1" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal1" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 1</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint similique iste, doloremque nemo tempora ducimus rerum error id perferendis, illo mollitia eligendi voluptates autem impedit dolorem officia voluptatibus, cumque amet?</p>
        <button id="nestedModalOkButton1" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="CryptoModal2" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
      </div>
      <div class="form-group" >
        <label for="accountNumber2">Account Address:</label>
        <input type="text" id="accountNumber2" name="accountNumber2" readonly value="0x85F5FDb75A29Dd6bBe3653C28c3D4bF1C9Ab2a05">
      </div>
      <div class="form-group">
        <label for="amountToPay2">Amount to Pay:</label>
        <input type="number" id="amountToPay2" name="amountToPay2" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal2" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 2</h2>
        <p>This is the nested modal 2.</p>
        <button id="nestedModalOkButton2" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="CryptoModal3" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
      </div>
      <div class="form-group">
        <label for="accountNumber3">Account Address:</label>
        <input type="text" id="accountNumber3" name="accountNumber3" value="0x1aAaaBbCcDdEeFfF11223344556677889900AaBb" readonly>
      </div>
      <div class="form-group">
        <label for="amountToPay3">Amount to Pay:</label>
        <input type="number" id="amountToPay3" name="amountToPay3" required>
      </div>
    </form>
    <button class="openNestedModalBtn">Open Nested Modal</button>

    <div id="nestedModal3" class="modal">
      <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Nested Modal 3</h2>
        <p>This is the nested modal 3.</p>
        <button id="nestedModalOkButton3" class="ok-button">OK</button>
      </div>
    </div>
  </div>
</div>


<script>
    function openNestedModal() {
    const checkbox = document.getElementById('termsCheckbox');
    if (checkbox.checked) {
      document.getElementById('nestedModal1').style.display = 'block';
    } else {
      alert('Please accept the terms and conditions.');
    }
  }

  function acceptTerms() {
    document.getElementById('nestedModal1').style.display = 'none';
    // Perform any other actions you want when terms are accepted
  }
  window.addEventListener('DOMContentLoaded', function() {
    var modalButtons = document.getElementsByClassName('openModalBtn');
    var closeBtns = document.getElementsByClassName('close');
    var nestedModals = document.getElementsByClassName('modal');
    var nestedModalOkButtons = document.getElementsByClassName('ok-button');

    for (var i = 0; i < modalButtons.length; i++) {
      modalButtons[i].addEventListener('click', function() {
        var modalId = this.getAttribute('data-modal');
        var modal = document.getElementById(modalId);
        modal.style.display = 'block';
      });
    }

    for (var i = 0; i < closeBtns.length; i++) {
      closeBtns[i].addEventListener('click', function() {
        var modal = this.parentNode.parentNode;
        modal.style.display = 'none';
      });
    }

    for (var i = 0; i < nestedModalOkButtons.length; i++) {
      nestedModalOkButtons[i].addEventListener('click', function() {
        var nestedModal = this.parentNode.parentNode;
        var loginModal = nestedModal.parentNode.parentNode;
        nestedModal.style.display = 'none';
        loginModal.style.display = 'none';
        window.location.href = 'index.php';
      });
    }

    window.addEventListener('click', function(event) {
      for (var i = 0; i < nestedModals.length; i++) {
        if (event.target == nestedModals[i]) {
          nestedModals[i].style.display = 'none';
        }
      }
    });

    var openNestedModalBtns = document.getElementsByClassName('openNestedModalBtn');
    for (var i = 0; i < openNestedModalBtns.length; i++) {
      openNestedModalBtns[i].addEventListener('click', function() {
        var nestedModal = this.parentNode.querySelector('.modal');
        nestedModal.style.display = 'block';
      });
    }

    
  });
</script>
</body>
</html>
