<?php
// Connect to the MySQL database "users" (replace with your database credentials)
include '../admin/inc/db_config.php';

// Retrieve user data from the "user" table
$sql = "SELECT * FROM user";
$result = $con->query($sql);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Reference Numbers</title>
</head>
<body>
    <h1>Reference Numbers</h1>
    <table border="1">
        <tr>
            <th>Order No.</th>
            <th>Sender's Name</th>
            <th>Reference Number</th>
        </tr>
        <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["name"] . "</td>";
                echo "<td>" . $row["number"] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='3'>No user data available</td></tr>";
        }
        $con->close();
        ?>
    </table>
</body>
</html>
