<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Get email subject and body from the form
    $emailSubject = $_POST['emailSubject'];
    $emailBody = $_POST['emailBody'];
}
// Database connection settings
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "capstone";

// Create a connection to the database
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Retrieve email addresses from the database
$sql = "SELECT email FROM user_cred";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Initialize PHPMailer
    $mail = new PHPMailer(true);
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'jamescanedo1@gmail.com';                     //SMTP username
    $mail->Password   = 'hehgtrrelfllahnq';                               //SMTP password
    $mail->SMTPSecure = 'tls';                                     //Enable implicit TLS encryption
    $mail->Port       = 587; 

    // Email settings
    $mail->setFrom('jamescanedo1@gmail.com', 'ARGAO ECO BAY PARK');
    $mail->isHTML(true);
    $mail->Subject = $emailSubject;
    $mail->Body = $emailBody;

    $emailSent = false;

    while ($row = $result->fetch_assoc()) {
        $recipient_email = $row['email'];
        $mail->addAddress($recipient_email);

        // Send the email
        try {
            $mail->send();
            $emailSent = true;
            $mail->clearAddresses(); // Clear recipients for the next iteration
            
        } catch (Exception $e) {
            echo "Error sending email to $recipient_email: " . $mail->ErrorInfo . "<br>";
        }
    }

    if ($emailSent) {
        echo '<script>alert("Emails were sent successfully."); window.location.href = "../users.php";</script>';
    }
} else {
    echo "Invalid request.";
}

// Close the database connection
$conn->close();
?>