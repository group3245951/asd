<?php
//custom functions and configurations for twilio
require_once "functions.php";
require_once "../ajax/new_bookings.php";
if (!empty($_POST['phone']) && !empty($_POST['message'])) {

  $phone = $_POST['phone'];
  $message = $_POST['message'];
  $attempt = sendSMS($message, $phone);
  dump($attempt);
}

?>
<!-- 
<form action="" method="post">
  <label for="">Phone number</label><br>
  <input type="text" name="phone" required>

  <br><br>
  <label for="">Message</label><br>
  <textarea name="message" id="" cols="80" rows="2" required></textarea>

  <br><br>
  <input type="submit" value="Send">
</form> -->

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Send Message</title>
  <link rel="stylesheet" href="index.css">
  <link href="https://fonts.googleapis.com/css2?family=Calistoga&display=swap" rel="stylesheet">
</head>

<body>
  
  <form action="" method="post">
    <label for="">Phone number</label><br>
    <input type="text" name="phone" placeholder="+63" required>

    <br><br>
<label for="message">Your Message</label><br>
<textarea name="message" id="" cols="40" rows="5" required>
Thank you for choosing Argao Eco Bay Park Resort Hotel! We are delighted to confirm your booking.

Booking Details:
- Room Type: 
- Check-in Date: 
- Check-out Date: 

We look forward to welcoming you. If you have any further questions or special requests, feel free to let us know.

Best regards,
Argao Bay Eco-Park Resort Hotel
</textarea>


    <br><br>
    <button type="submit">
  <div class="svg-wrapper-1">
    <div class="svg-wrapper">
      <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0h24v24H0z" fill="none"></path>
        <path d="M1.946 9.315c-.522-.174-.527-.455.01-.634l19.087-6.362c.529-.176.832.12.684.638l-5.454 19.086c-.15.529-.455.547-.679.045L12 14l6-8-8 6-8.054-2.685z" fill="currentColor"></path>
      </svg>
    </div>
  </div>
  <span>Send</span>
</button>
  </form>
</body>

</html>